<?php
/**
 * @file
 * datacash_reports.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function datacash_reports_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'dc_tnx_detail_report';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'datacash_report';
  $view->human_name = 'dc_tnx_detail_report';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Datacash Transaction Detail';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Datacash Report: Internal_reference */
  $handler->display->display_options['fields']['internal_reference']['id'] = 'internal_reference';
  $handler->display->display_options['fields']['internal_reference']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['internal_reference']['field'] = 'internal_reference';
  $handler->display->display_options['fields']['internal_reference']['label'] = 'Internal Reference';
  /* Field: Datacash Report: Ref */
  $handler->display->display_options['fields']['ref']['id'] = 'ref';
  $handler->display->display_options['fields']['ref']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['ref']['field'] = 'ref';
  $handler->display->display_options['fields']['ref']['label'] = 'Merchant Reference';
  /* Field: Datacash Report: Tnx_date */
  $handler->display->display_options['fields']['tnx_date']['id'] = 'tnx_date';
  $handler->display->display_options['fields']['tnx_date']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['tnx_date']['field'] = 'tnx_date';
  $handler->display->display_options['fields']['tnx_date']['label'] = 'Transaction Date';
  /* Field: Datacash Report: Amount */
  $handler->display->display_options['fields']['amount']['id'] = 'amount';
  $handler->display->display_options['fields']['amount']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['amount']['field'] = 'amount';
  $handler->display->display_options['fields']['amount']['precision'] = '0';
  /* Field: Datacash Report: Currency */
  $handler->display->display_options['fields']['currency']['id'] = 'currency';
  $handler->display->display_options['fields']['currency']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['currency']['field'] = 'currency';
  /* Field: Datacash Report: Card_type */
  $handler->display->display_options['fields']['card_type']['id'] = 'card_type';
  $handler->display->display_options['fields']['card_type']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['card_type']['field'] = 'card_type';
  $handler->display->display_options['fields']['card_type']['label'] = 'Card Type';
  /* Field: Datacash Report: Country_of_issue */
  $handler->display->display_options['fields']['country_of_issue']['id'] = 'country_of_issue';
  $handler->display->display_options['fields']['country_of_issue']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['country_of_issue']['field'] = 'country_of_issue';
  $handler->display->display_options['fields']['country_of_issue']['label'] = 'Country of Issue';
  /* Field: Datacash Report: Response */
  $handler->display->display_options['fields']['response']['id'] = 'response';
  $handler->display->display_options['fields']['response']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['response']['field'] = 'response';
  /* Field: Datacash Report: Red_Response */
  $handler->display->display_options['fields']['red_Response']['id'] = 'red_Response';
  $handler->display->display_options['fields']['red_Response']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['red_Response']['field'] = 'red_Response';
  $handler->display->display_options['fields']['red_Response']['label'] = 'Red Response';
  /* Field: Datacash Report: Bank_response */
  $handler->display->display_options['fields']['bank_response']['id'] = 'bank_response';
  $handler->display->display_options['fields']['bank_response']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['bank_response']['field'] = 'bank_response';
  $handler->display->display_options['fields']['bank_response']['label'] = 'Bank Response';
  /* Field: Datacash Report: Cv2avs_response */
  $handler->display->display_options['fields']['cv2avs_response']['id'] = 'cv2avs_response';
  $handler->display->display_options['fields']['cv2avs_response']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['cv2avs_response']['field'] = 'cv2avs_response';
  $handler->display->display_options['fields']['cv2avs_response']['label'] = 'Cv2avs Response';
  /* Field: Datacash Report: Clearing_house */
  $handler->display->display_options['fields']['clearing_house']['id'] = 'clearing_house';
  $handler->display->display_options['fields']['clearing_house']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['clearing_house']['field'] = 'clearing_house';
  $handler->display->display_options['fields']['clearing_house']['label'] = 'Clearing House';
  /* Field: Datacash Report: Issuing_bank */
  $handler->display->display_options['fields']['issuing_bank']['id'] = 'issuing_bank';
  $handler->display->display_options['fields']['issuing_bank']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['issuing_bank']['field'] = 'issuing_bank';
  $handler->display->display_options['fields']['issuing_bank']['label'] = 'Issuing Bank';
  /* Field: Datacash Report: Policy */
  $handler->display->display_options['fields']['policy']['id'] = 'policy';
  $handler->display->display_options['fields']['policy']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['policy']['field'] = 'policy';
  /* Field: Datacash Report: Settlement_date */
  $handler->display->display_options['fields']['settlement_date']['id'] = 'settlement_date';
  $handler->display->display_options['fields']['settlement_date']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['settlement_date']['field'] = 'settlement_date';
  $handler->display->display_options['fields']['settlement_date']['label'] = 'Settlement Date';
  /* Field: Datacash Report: Street_address_1 */
  $handler->display->display_options['fields']['street_address_1']['id'] = 'street_address_1';
  $handler->display->display_options['fields']['street_address_1']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['street_address_1']['field'] = 'street_address_1';
  $handler->display->display_options['fields']['street_address_1']['label'] = 'Street Address 1';
  /* Field: Datacash Report: Street_address_2 */
  $handler->display->display_options['fields']['street_address_2']['id'] = 'street_address_2';
  $handler->display->display_options['fields']['street_address_2']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['street_address_2']['field'] = 'street_address_2';
  $handler->display->display_options['fields']['street_address_2']['label'] = 'Street Address 2';
  /* Field: Datacash Report: Street_address_3 */
  $handler->display->display_options['fields']['street_address_3']['id'] = 'street_address_3';
  $handler->display->display_options['fields']['street_address_3']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['street_address_3']['field'] = 'street_address_3';
  $handler->display->display_options['fields']['street_address_3']['label'] = 'Street Address 3';
  /* Field: Datacash Report: Street_address_4 */
  $handler->display->display_options['fields']['street_address_4']['id'] = 'street_address_4';
  $handler->display->display_options['fields']['street_address_4']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['street_address_4']['field'] = 'street_address_4';
  $handler->display->display_options['fields']['street_address_4']['label'] = 'Street Address 4';
  /* Field: Datacash Report: Postcode */
  $handler->display->display_options['fields']['postcode']['id'] = 'postcode';
  $handler->display->display_options['fields']['postcode']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['postcode']['field'] = 'postcode';
  /* Field: Datacash Report: Threed_secure_registered */
  $handler->display->display_options['fields']['threed_secure_registered']['id'] = 'threed_secure_registered';
  $handler->display->display_options['fields']['threed_secure_registered']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['threed_secure_registered']['field'] = 'threed_secure_registered';
  $handler->display->display_options['fields']['threed_secure_registered']['label'] = 'Threed Secure Registered';
  /* Field: Datacash Report: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  /* Field: Datacash Report: Environment */
  $handler->display->display_options['fields']['environment']['id'] = 'environment';
  $handler->display->display_options['fields']['environment']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['environment']['field'] = 'environment';
  /* Field: Datacash Report: Vtid */
  $handler->display->display_options['fields']['vtid']['id'] = 'vtid';
  $handler->display->display_options['fields']['vtid']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['vtid']['field'] = 'vtid';
  $handler->display->display_options['fields']['vtid']['label'] = 'vTid';
  $handler->display->display_options['fields']['vtid']['separator'] = '';
  /* Contextual filter: Datacash Report: Internal_reference */
  $handler->display->display_options['arguments']['internal_reference']['id'] = 'internal_reference';
  $handler->display->display_options['arguments']['internal_reference']['table'] = 'datacash_report';
  $handler->display->display_options['arguments']['internal_reference']['field'] = 'internal_reference';
  $handler->display->display_options['arguments']['internal_reference']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['internal_reference']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['internal_reference']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['internal_reference']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['internal_reference']['limit'] = '0';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $export['dc_tnx_detail_report'] = $view;

  $view = new view();
  $view->name = 'dc_tnx_summary_report';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'datacash_report';
  $view->human_name = 'dc_tnx_summary_report';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Transaction Summary';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'amount' => 'amount',
    'bank_response' => 'bank_response',
    'card_type' => 'card_type',
    'id_1' => 'id_1',
    'internal_reference' => 'internal_reference',
    'ref' => 'ref',
    'response' => 'response',
    'tnx_date' => 'tnx_date',
    'vtid' => 'vtid',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'amount' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'bank_response' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'card_type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'id_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'internal_reference' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'ref' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'response' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'tnx_date' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'vtid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Datacash Report: Internal_reference */
  $handler->display->display_options['fields']['internal_reference']['id'] = 'internal_reference';
  $handler->display->display_options['fields']['internal_reference']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['internal_reference']['field'] = 'internal_reference';
  $handler->display->display_options['fields']['internal_reference']['label'] = 'Internal Reference';
  $handler->display->display_options['fields']['internal_reference']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['internal_reference']['alter']['path'] = 'datacash-reports/tnx-detail/[internal_reference]';
  $handler->display->display_options['fields']['internal_reference']['alter']['link_class'] = 'use-ajax';
  /* Field: Datacash Report: Ref */
  $handler->display->display_options['fields']['ref']['id'] = 'ref';
  $handler->display->display_options['fields']['ref']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['ref']['field'] = 'ref';
  $handler->display->display_options['fields']['ref']['label'] = 'Merchant Reference';
  /* Field: Datacash Report: Tnx_date */
  $handler->display->display_options['fields']['tnx_date']['id'] = 'tnx_date';
  $handler->display->display_options['fields']['tnx_date']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['tnx_date']['field'] = 'tnx_date';
  $handler->display->display_options['fields']['tnx_date']['label'] = 'Transaction Date';
  /* Field: Datacash Report: Amount */
  $handler->display->display_options['fields']['amount']['id'] = 'amount';
  $handler->display->display_options['fields']['amount']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['amount']['field'] = 'amount';
  $handler->display->display_options['fields']['amount']['precision'] = '0';
  /* Field: Datacash Report: Card_type */
  $handler->display->display_options['fields']['card_type']['id'] = 'card_type';
  $handler->display->display_options['fields']['card_type']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['card_type']['field'] = 'card_type';
  $handler->display->display_options['fields']['card_type']['label'] = 'Card Type';
  /* Field: Datacash Report: Bank_response */
  $handler->display->display_options['fields']['bank_response']['id'] = 'bank_response';
  $handler->display->display_options['fields']['bank_response']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['bank_response']['field'] = 'bank_response';
  $handler->display->display_options['fields']['bank_response']['label'] = 'Bank Response';
  /* Field: Datacash Report: Response */
  $handler->display->display_options['fields']['response']['id'] = 'response';
  $handler->display->display_options['fields']['response']['table'] = 'datacash_report';
  $handler->display->display_options['fields']['response']['field'] = 'response';
  /* Filter criterion: Datacash Report: Internal_reference */
  $handler->display->display_options['filters']['internal_reference']['id'] = 'internal_reference';
  $handler->display->display_options['filters']['internal_reference']['table'] = 'datacash_report';
  $handler->display->display_options['filters']['internal_reference']['field'] = 'internal_reference';
  $handler->display->display_options['filters']['internal_reference']['exposed'] = TRUE;
  $handler->display->display_options['filters']['internal_reference']['expose']['operator_id'] = 'internal_reference_op';
  $handler->display->display_options['filters']['internal_reference']['expose']['label'] = 'Internal Reference';
  $handler->display->display_options['filters']['internal_reference']['expose']['operator'] = 'internal_reference_op';
  $handler->display->display_options['filters']['internal_reference']['expose']['identifier'] = 'internal_reference';
  $handler->display->display_options['filters']['internal_reference']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Datacash Report: Ref */
  $handler->display->display_options['filters']['ref']['id'] = 'ref';
  $handler->display->display_options['filters']['ref']['table'] = 'datacash_report';
  $handler->display->display_options['filters']['ref']['field'] = 'ref';
  $handler->display->display_options['filters']['ref']['exposed'] = TRUE;
  $handler->display->display_options['filters']['ref']['expose']['operator_id'] = 'ref_op';
  $handler->display->display_options['filters']['ref']['expose']['label'] = 'Merchant Reference';
  $handler->display->display_options['filters']['ref']['expose']['operator'] = 'ref_op';
  $handler->display->display_options['filters']['ref']['expose']['identifier'] = 'ref';
  $handler->display->display_options['filters']['ref']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Datacash Report: Card_type */
  $handler->display->display_options['filters']['card_type']['id'] = 'card_type';
  $handler->display->display_options['filters']['card_type']['table'] = 'datacash_report';
  $handler->display->display_options['filters']['card_type']['field'] = 'card_type';
  $handler->display->display_options['filters']['card_type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['card_type']['expose']['operator_id'] = 'card_type_op';
  $handler->display->display_options['filters']['card_type']['expose']['label'] = 'Card Type';
  $handler->display->display_options['filters']['card_type']['expose']['operator'] = 'card_type_op';
  $handler->display->display_options['filters']['card_type']['expose']['identifier'] = 'card_type';
  $handler->display->display_options['filters']['card_type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Datacash Report: Tnx_date */
  $handler->display->display_options['filters']['tnx_date']['id'] = 'tnx_date';
  $handler->display->display_options['filters']['tnx_date']['table'] = 'datacash_report';
  $handler->display->display_options['filters']['tnx_date']['field'] = 'tnx_date';
  $handler->display->display_options['filters']['tnx_date']['operator'] = 'contains';
  $handler->display->display_options['filters']['tnx_date']['exposed'] = TRUE;
  $handler->display->display_options['filters']['tnx_date']['expose']['operator_id'] = 'tnx_date_op';
  $handler->display->display_options['filters']['tnx_date']['expose']['label'] = 'Transaction Date';
  $handler->display->display_options['filters']['tnx_date']['expose']['operator'] = 'tnx_date_op';
  $handler->display->display_options['filters']['tnx_date']['expose']['identifier'] = 'tnx_date';
  $handler->display->display_options['filters']['tnx_date']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['block_description'] = 'dc_tnx_summary_report';
  $export['dc_tnx_summary_report'] = $view;

  return $export;
}
