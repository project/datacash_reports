<?php
/**
 * @file
 * Provide datacash reports helper functions.
 */

composer_manager_register_autoloader();
use \DatacashReports\DatacashReportManager;
use gchart\gPie3DChart;

/**
 * Fetches datacash report entries from datacash.
 *
 * @param string $start_date
 *   Report start date.
 * @param string $end_date
 *   Report end date.
 * @param string $tnx_type
 *   Report transaction type.
 */
function datacash_reports_get($start_date, $end_date, $tnx_type) {
  try {
    $environment = variable_get('datacash_reports_operation_environment');
    $manager = new DatacashReportManager();
    $manager->getConfigurator()->setEndPoint(variable_get('datacash_reports_url', 'https://testserver.datacash.com/reporting2/csvlist'))
      ->setGroup(variable_get('datacash_reports_group_id'))
      ->setUser(variable_get('datacash_reports_user_id'))
      ->setPassword(variable_get('datacash_reports_password'))
      ->setStartDate($start_date)
      ->setEndDate($end_date)
      ->setType($tnx_type);
    $response = $manager->getController($environment)->download();
    $response = explode(PHP_EOL, trim($response));
    array_shift($response);
    foreach ($response as $row) {
      datacash_reports_create(str_getcsv($row));
    }
  }
  catch (Exception $e) {
    drupal_set_message(t('There was an error in the report download request. Check the report download setting and search parameters.'));
    watchdog_exception('composer_manager', $e);
  }
}

/**
 * Create a datacash_report entity.
 *
 * @param array $values
 *   Array of values for entity.
 */
function datacash_reports_create(array $values) {
  // Get the field labels.
  $field_labels = datacash_reports_get_fileld_labels();

  // Create key-value paired associative array for entity values.
  $entity_values = array();
  foreach ($values as $index => $entity_data) {
    $entity_values[$field_labels[$index]] = $entity_data;
  }
  $entity = entity_create('datacash_report', $entity_values);
  $entity->save();
}

/**
 * Provides date string.
 *
 * @param array $date
 *   Date value in array format of date type field.
 * @param string $time_string
 *   Time in string format of h:i:s.
 *
 * @return string
 *   Date string in Y-m-d h:i:s format.
 */
function datacash_reports_get_query_date(array $date, $time_string) {
  return $date['year'] . "-" . str_pad($date['month'], 2, '0', STR_PAD_LEFT) . "-" . $date['day'] . " " . $time_string;
}

/**
 * Removes all entities of type datacash_report.
 */
function datacash_reports_truncate() {
  // Get the total count of entities.
  $query = new EntityFieldQuery();
  $count = $query->entityCondition('entity_type', 'datacash_report')
    ->count()
    ->execute();

  // Execute truncate in batches.
  $batch_size = variable_get('datacsh_reports_truncate_batch_size', 100);
  for ($index = 0; $index < ceil($count / $batch_size); $index++) {
    $entity_ids = array();
    $query = new EntityFieldQuery();
    $results = $query->entityCondition('entity_type', 'datacash_report')
      ->range(($index * $batch_size), $batch_size)
      ->execute();
    if (!empty($results['datacash_report'])) {
      foreach ($results['datacash_report'] as $entity) {
        $entity_ids[] = $entity->id;
      }
    }

    // Delete the entities.
    entity_delete_multiple('datacash_report', $entity_ids);
  }
}

/**
 * Provides array of field labels.
 *
 * @return array
 *   Array of field labels.
 */
function datacash_reports_get_fileld_labels() {
  // Get the default field labels.
  $default_field_labels = drupal_get_schema('datacash_report');
  $default_field_labels = $default_field_labels['fields'];
  // Remove the first field i.e. the primary key "id" field.
  array_shift($default_field_labels);
  // Get the keys only as field labels.
  $default_field_labels = array_keys($default_field_labels);

  // Prepare the list of final field labels based on configured values.
  $field_labels = variable_get('datacash_reports_entity_field_labels', $default_field_labels);
  return $field_labels;
}

/**
 * Provides transaction statistics.
 */
function datacash_reports_get_stats() {
  $statistics = array();

  // Get the total count of transactions.
  $query = new EntityFieldQuery();
  $statistics['total_count'] = $query->entityCondition('entity_type', 'datacash_report')
    ->count()
    ->execute();

  // Get more statistics only if there are some transactions.
  if ($statistics['total_count'] > 0) {
    // Get settled transactions.
    $query = new EntityFieldQuery();
    $statistics['pie_all']['data']['settled'] = $query->entityCondition('entity_type', 'datacash_report')
      ->propertyCondition('response', 'OK, Settled')
      ->count()
      ->execute();
    $statistics['pie_all']['data']['not_settled'] = $statistics['total_count'] - $statistics['pie_all']['data']['settled'];

    // Draw the pie chart for settled and not settled transactions.
    $default_width = variable_get('datacash_reports_chart_default_width', 500);
    $default_height = variable_get('datacash_reports_chart_default_height', 200);
    $pi_chart = new gPie3DChart($default_width, $default_height);
    $pi_chart->addDataSet($statistics['pie_all']['data']);
    $pi_chart->setLabels = array(t('Settled'), t('Not Settled'));
    $pi_chart->setLegend(array(t('Settled'), t('Not Settled')));
    $statistics['pie_all']['url'] = html_entity_decode($pi_chart->getUrl());

    // Get the transaction counts grouped by response and
    // those that are not settled.
    $query = new EntityFieldQuery();
    $not_settled_tnx = $query->entityCondition('entity_type', 'datacash_report')
      ->propertyCondition('response', 'OK, Settled', '<>')
      ->execute();
    $not_settled_tnx = entity_load('datacash_report', array_keys($not_settled_tnx['datacash_report']));
    foreach ($not_settled_tnx as $tnx) {
      $response = str_replace(' ', '_', $tnx->response);
      $response = str_replace(',', '', $response);
      if (empty($statistics['pie_not_settled']['data'][$response])) {
        $labels[] = $tnx->response;
        $statistics['pie_not_settled']['data'][$response] = 0;
      }
      $statistics['pie_not_settled']['data'][$response]++;
    }

    // Draw the pie chart for not settled transactions.
    $pi_chart = new gPie3DChart($default_width, $default_height);
    $pi_chart->addDataSet($statistics['pie_not_settled']['data']);
    $pi_chart->setLabels = $labels;
    $pi_chart->setLegend($labels);
    $statistics['pie_not_settled']['url'] = html_entity_decode($pi_chart->getUrl());
  }

  return $statistics;
}

/**
 * Provides default date values.
 */
function datacash_reports_get_default_date_values($type = 'datacash_reports_last_chosen_start_date') {
  $default = array(
    'year' => date('Y'),
    'month' => date('n'),
    'day' => date('j'),
  );
  return variable_get($type, $default);
}
