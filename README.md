Datacash Reports
================
This module Provides integration of datacash payment transaction reports from
datacash to be displayed and analysed locally in your website. To use this
module just install it and go to the configuration page
(admin/config/system/daracash-reports/settings).

Set the following as a must for the module to work:
Datacsh account group id.:
Datacsh account client/user id.:
Datacsh report access password:
Datacsh report download URL:
Datacsh report operation environment:
(This can be either dev/prod, dev works with stub and prod would
download the actual report from datacash)


The datacash reporting credentials is usually available whenever
an account is setup.
For more info on datacash reporting please check the following links:
https://testserver.datacash.com/software/integration.shtml
http://www.mastercard.com/gateway/implementation_guides/index.html
