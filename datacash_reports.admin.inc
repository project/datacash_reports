<?php
/**
 * @file
 * Provides admin settings for datacash reports.
 */

/**
 * Form constructor for datacash reports settings from.
 */
function datacash_reports_settings_form($form, &$form_state) {
  $form['datacash_reports_group_id'] = array(
    '#title' => 'Datacsh account group id.',
    '#type' => 'textfield',
    '#default_value' => variable_get('datacash_reports_group_id'),
    '#description' => t('Datacash account group id.'),
  );

  $form['datacash_reports_user_id'] = array(
    '#title' => 'Datacsh account client/user id.',
    '#type' => 'textfield',
    '#default_value' => variable_get('datacash_reports_user_id'),
    '#description' => t('Datacash account client/user id.'),
  );

  $form['datacash_reports_password'] = array(
    '#title' => 'Datacsh report access password',
    '#type' => 'textfield',
    '#default_value' => variable_get('datacash_reports_password'),
    '#description' => t('Datacash report access password.'),
  );

  $form['datacash_reports_url'] = array(
    '#title' => 'Datacsh report download URL',
    '#type' => 'textfield',
    '#default_value' => variable_get('datacash_reports_url', 'https://testserver.datacash.com/reporting2/csvlist'),
    '#description' => t('Datacash report download URL.'),
  );

  $form['datacash_reports_operation_environment'] = array(
    '#title' => 'Datacsh report operation environment',
    '#type' => 'textfield',
    '#default_value' => variable_get('datacash_reports_operation_environment', 'dev'),
    '#description' => t('Datacash report operation environment.'),
  );

  $form['datacash_reports_chart_default_width'] = array(
    '#title' => 'Datacsh report default chart width',
    '#type' => 'textfield',
    '#default_value' => variable_get('datacash_reports_chart_default_width', 500),
    '#description' => t('Datacsh report default chart width.'),
  );

  $form['datacash_reports_chart_default_height'] = array(
    '#title' => 'Datacsh report default chart height',
    '#type' => 'textfield',
    '#default_value' => variable_get('datacash_reports_chart_default_height', 200),
    '#description' => t('Datacsh report default chart height.'),
  );

  return system_settings_form($form);
}
