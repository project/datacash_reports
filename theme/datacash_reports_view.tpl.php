<?php
/**
 * @file
 * Provides template for displaying report.
 */
?>
<div id="datacash_reports_view">
  <fieldset class="datacash_reports_view_container">
    <legend>
      <span><?php print t('Transaction Summary'); ?></span>
    </legend>
    <div><?php print $report; ?></div>
    <div id="dcr-tnx-detail"></div>
  </fieldset>
</div>
