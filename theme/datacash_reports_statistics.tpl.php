<?php
/**
 * @file
 * Provides template for displaying statistics for datacash reports.
 */
?>
<div id="datacash_reports_statistics">
  <fieldset class="datacash_reports_statistics_container">
    <legend>
      <span><?php print t('Transaction Statistics'); ?></span>
    </legend>
    <div id="pie_all">
      <div><?php print t('All Transactions'); ?></div>
      <?php print $charts['pie_all']; ?>
    </div>
    <div id="pie_not_settled">
      <div><?php print t('Transactions not Settled'); ?></div>
      <?php print $charts['pie_not_settled']; ?>
    </div>
  </fieldset>
</div>
