<?php
/**
 * @file
 * datacash_reports.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function datacash_reports_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'datacash_reports_landing';
  $page->task = 'page';
  $page->admin_title = 'datacash_reports_landing';
  $page->admin_description = 'Datacash reports landing page';
  $page->path = 'datacash-reports';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => datacash_reports_get_administrator_role_id(),
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Datacash Reports',
    'weight' => '0',
    'name' => 'navigation',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'weight' => 0,
      'name' => 'navigation',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_datacash_reports_landing__panel_context_5c442ecd-428e-4160-85cc-5e32099e2c38';
  $handler->task = 'page';
  $handler->subtask = 'datacash_reports_landing';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Landing page',
    'no_blocks' => FALSE,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'twocol_bricks';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left_above' => NULL,
      'right_above' => NULL,
      'middle' => NULL,
      'left_below' => NULL,
      'right_below' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Datacash Reports';
  $display->uuid = 'ab73c66b-5872-472c-859e-edad447e1402';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-5baeb277-46f6-4c52-ba7a-6ffafe0265b7';
  $pane->panel = 'bottom';
  $pane->type = 'block';
  $pane->subtype = 'datacash_reports-datacash_reports_view';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5baeb277-46f6-4c52-ba7a-6ffafe0265b7';
  $display->content['new-5baeb277-46f6-4c52-ba7a-6ffafe0265b7'] = $pane;
  $display->panels['bottom'][0] = 'new-5baeb277-46f6-4c52-ba7a-6ffafe0265b7';
  $pane = new stdClass();
  $pane->pid = 'new-2ca46bdc-267d-4e53-b5f0-77e1625d5d7d';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'datacash_reports-datacash_reports_statistics_view';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '2ca46bdc-267d-4e53-b5f0-77e1625d5d7d';
  $display->content['new-2ca46bdc-267d-4e53-b5f0-77e1625d5d7d'] = $pane;
  $display->panels['middle'][0] = 'new-2ca46bdc-267d-4e53-b5f0-77e1625d5d7d';
  $pane = new stdClass();
  $pane->pid = 'new-9c6ca894-739e-4617-9e93-209f2f83c679';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'datacash_reports-datacash_report_download_form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9c6ca894-739e-4617-9e93-209f2f83c679';
  $display->content['new-9c6ca894-739e-4617-9e93-209f2f83c679'] = $pane;
  $display->panels['top'][0] = 'new-9c6ca894-739e-4617-9e93-209f2f83c679';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['datacash_reports_landing'] = $page;

  return $pages;

}

/**
 * Returns administrator role id.
 *
 * @return int
 *   Administrator role id.
 */
function datacash_reports_get_administrator_role_id() {
  $roles = user_roles();
  return array_search('administrator', $roles);
}
