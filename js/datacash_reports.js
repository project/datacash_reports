/**
 * @file
 * Provides jQuery for refresh of charts after new download.
 */

(function ($) {
  "use strict";
  Drupal.behaviors.datacash_reports = {
    attach: function (context, settings) {
      $('#tnx-detail-back').click(function () {
        $('.view-dc-tnx-summary-report').show();
        $('#dcr-tnx-detail').html('');
      });
    }
  };
}(jQuery));
